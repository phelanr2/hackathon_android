package com.example.hackathonapp;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void enter_payment2user_select_pay(View view) {
		// payment stuff
		setContentView(R.layout.user_select_pay);
	}

	public void user_select_pay2user_select_card(View view) {
		// nothing
		setContentView(R.layout.user_select_card);
	}

	public void user_select_card2user_pay_result(View view) {
		// send up details to server
		setContentView(R.layout.user_pay_result);
	}

	public void user_pay_result2home(View view) {
		// send up details to server
		setContentView(R.layout.enter_payment);
	}

	public void login2enter_payment(View view) {
		EditText name = (EditText)findViewById(R.id.homeUserName);
		EditText pass = (EditText)findViewById(R.id.homePassword);
		String[] userAndPass = new String[2];
		userAndPass[0] = name.getText().toString();
		userAndPass[1] = pass.getText().toString();
		HttpAsyncLogIn x = new HttpAsyncLogIn();
		x.execute();
		setContentView(R.layout.home_screen);

	}
}

// START ASYNC
class HttpAsyncLogIn extends AsyncTask<String, String, String> {
	@Override
	protected String doInBackground(String... params1) {
		String response = null;
		try {
			HttpParams params = new BasicHttpParams();
			HttpConnectionParams.setSoTimeout(params, 0);
			HttpClient httpClient = new DefaultHttpClient(params);
			String urlString = "http://10.157.194.189:8090/GrailsProject/hackapi/logIn";

			HttpPost httpPost = new HttpPost(urlString);

		    ArrayList<NameValuePair> postParameters;
		    postParameters = new ArrayList<NameValuePair>();
		    //GET NAME AND PASS
		    String user = params1[0];
		    String password = params1[1];
		    postParameters.add(new BasicNameValuePair("username", user));
		    postParameters.add(new BasicNameValuePair("password", password));

		    httpPost.setEntity(new UrlEncodedFormEntity(postParameters));

		    HttpEntity entity = httpClient.execute(httpPost).getEntity();
			response = EntityUtils.toString(entity);
			httpClient.getConnectionManager().shutdown();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
        super.onPostExecute(result);
        try {
            JSONArray jsonarray = new JSONArray(result);
//          JSONObject jb = (JSONObject) jsonarray.getJSONObject(0);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
		}
	}
}
// END ASYNC